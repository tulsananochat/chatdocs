XSLTPROC=`which xsltproc`
EPUB=/usr/local/share/xsl/docbook-ns/epub/docbook.xsl
HTML=/usr/local/share/xsl/docbook-ns/html/docbook.xsl

html : commands.xml
		${XSLTPROC} -o commands.html ${HTML} commands.xml

epub : commands.xml
		${XSLTPROC} -o commands.epub ${EPUB} commands.xml
